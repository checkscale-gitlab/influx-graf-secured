# Secured InfluxDB + Grafana
Secured Influx+Grafana Containers
Ready to be composed with provisionning of certificates, databases to create, dashboards.

### Launch
Edit ./setup/influxdb-config.json
Edit ./setup/grafana-config.json

```shell
# Clean persistence & Change rights for Grafana 
$ ./compose.sh clean

# Start containers (wait 15 seconds for influxdb provisioning)
$ ./compose.sh up

# Launch the setup
$ ./setup-influxdb.sh
$ ./setup-grafana.sh

# Stop containers
$ ./compose.sh down
```
Access influxdb on its port with client (8086 by default)

Access grafana with a web browser on its port (3000 by default).


### Setup

**Cleaning the environment**
- rm -rf ./persistence/grafana/* 
- rm -rf ./persistence/influxdb/*
- sudo chmod -R a+w ./persistence/grafana

**Influxdb Config**
- Network port :
    - in ./compose.yml : ports : "8086:8086" -> "NEW_PORT_OUTSIDE_CONTAINER:8086"
    - Careful in the following, between influx & grafana, the port used is the internal one to the docker network
- Databases creation : in ./setup/influxdb-setup.json
- Users admin + writers + readers : in ./setup/influxdb-setup.json

**Grafana Config**
- Network port
    - in ./compose.yml : ports : "3000:3000"
    - in ./grafana/grafana.ini for Grafana itself (3000) 
- Users : in ./setup/grafana-config.json
    - the default admin in ./grafana/grafana.ini will be deleted after setup
- Datasources + Dashboards : in ./setup/grafana-config.json & ./setup/
- Certificates :
    - put cert file in ./grafana/cert
    - modify http to https and path to cert file in ./grafana/grafana.ini
