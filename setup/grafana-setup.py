# coding=utf-8
import requests
import json
import pathlib

if __name__ == '__main__':
    fp               = pathlib.Path('grafana-config.json')
    conf             = json.loads(fp.read_text())
    graf_host        = conf['server_url']
    graf_org_id      = conf['organization_id']
    graf_admin       = conf['user_admin']
    graf_viewers     = conf['users_viewer']
    graf_editors     = conf['users_editor']
    graf_datasources = conf['datasources']

    # ---------- Grafana : Creation of new admin account ----------
    url = '{:s}/api/admin/users'.format(graf_host)
    query = { 'login': graf_admin['login'] , 'password' : graf_admin['passw'] , 'name' : graf_admin['login'] , 'email' : '{:s}@localhost'.format(graf_admin['login'])}
    print("\nGrafana : Creation of Admin account {:s}".format(graf_admin['login']))
    response = requests.post(url=url, json=query)
    print('  - Creation -> {:s}'.format(str(response.json())))
    admin_id = str(response.json().get('id') or None)
    if admin_id is not None :
        url = '{:s}/api/admin/users/{:s}/permissions'.format(graf_host, admin_id)
        query = { 'isGrafanaAdmin' : True }
        response = requests.put(url=url, json=query)
        print('  - IsAdmin -> {:s}'.format(str(response.json())))

        # --- Add in organization
        url = '{:s}/api/orgs/{:s}/users'.format(graf_host, graf_org_id)
        query = { 'loginOrEmail': graf_admin['login'] , 'role' : 'Admin' }
        response = requests.post(url=url, json=query)
        print('  - {:s} in Org -> {:s}'.format(graf_admin['login'], str(response.json())))

        # --- Update role in organization
        url = '{:s}/api/orgs/{:s}/users/{:s}'.format(graf_host, graf_org_id, admin_id)
        query = { 'role' : 'Admin' }
        response = requests.patch(url=url, data=query)
        print('  - {:s} as Admin in Org -> {:s}'.format(graf_admin['login'], str(response.json())))

    # ---------- Grafana : Creation of reader accounts ----------
    print("\nGrafana : Creation of Viewers")
    for leuser in graf_viewers :
        url = '{:s}/api/admin/users'.format(graf_host)
        query = { 'login': leuser['login'] , 'password' : leuser['passw'] , 'name' : leuser['login'] , 'email' : '{:s}@localhost'.format(leuser['login'])}
        response = requests.post(url=url, json=query)
        print('  - Creation -> {:s}'.format(str(response.json())))
        leuser_id = str(response.json().get('id') or None)
        leuser_login = leuser['login']
        if leuser_login is not None :
            # --- Add in organization
            url = '{:s}/api/orgs/{:s}/users'.format(graf_host, graf_org_id)
            query = { 'loginOrEmail': leuser_login , 'role' : 'Viewer' }
            response = requests.post(url=url, json=query)
            print('  - {:s} in Org -> {:s}'.format(leuser['login'], str(response.json())))

            # --- Update role in organization
            url = '{:s}/api/orgs/{:s}/users/{:s}'.format(graf_host, graf_org_id, leuser_id)
            query = { 'role' : 'Viewer' }
            response = requests.patch(url=url, data=query)
            print('  - {:s} as Viewer in Org -> {:s}'.format(leuser['login'], str(response.json())))

    # ---------- Grafana : Creation of reader accounts ----------
    print("\nGrafana : Creation of Editors")
    for leuser in graf_editors :
        url = '{:s}/api/admin/users'.format(graf_host)
        query = { 'login': leuser['login'] , 'password' : leuser['passw'] , 'name' : leuser['login'] , 'email' : '{:s}@localhost'.format(leuser['login'])}
        response = requests.post(url=url, json=query)
        print('  - Creation {:s} -> {:s}'.format(leuser['login'], str(response.json())))
        leuser_id    = str(response.json().get('id') or None)
        leuser_login = leuser['login']
        if leuser_login is not None :
            # --- Add in organization
            url = '{:s}/api/orgs/{:s}/users'.format(graf_host, graf_org_id)
            query = { 'loginOrEmail': leuser_login , 'role' : 'Editor' }
            response = requests.post(url=url, json=query)
            print('  - {:s} in Org -> {:s}'.format(leuser['login'], str(response.json())))

            # --- Update role in organization
            url = '{:s}/api/orgs/{:s}/users/{:s}'.format(graf_host, graf_org_id, leuser_id)
            query = { 'role' : 'Editor' }
            response = requests.patch(url=url, data=query)
            print('  - {:s} as Editor in Org -> {:s}'.format(leuser['login'], str(response.json())))

    # ---------- Grafana : Summary of users ----------
    url = '{:s}/api/users'.format(graf_host)
    query = { 'perpage': '100' , 'page' : '1'}
    print("\nGrafana : List of users {:s}".format(graf_admin['login']))
    response = requests.request("GET", url, params=query)
    les_users = response.json()
    for leuser in les_users :
        print('  -> {:s}'.format(str(leuser)))

    # ---------- Grafana : Gestion des organisations & teams ----------
    url = '{:s}/api/orgs/1/users'.format(graf_host)
    query = { }
    print("\nGrafana : Organisation")
    response = requests.request("GET", url, params=query)
    les_users = response.json()
    for leuser in les_users :
        print('  -> {:s}'.format(str(leuser)))

    # ---------- Grafana : Datasources ----------
    print("\nGrafana : Creation of Datasources")
    for lads in graf_datasources :
        url = '{:s}/api/datasources'.format(graf_host)
        query = lads
        response = requests.post(url=url, json=query)
        print('  - Creation {:s} -> {:s}'.format(lads['name'], str(response.json())))
        lads_id = str(response.json().get('id') or None)
        url = '{:s}/api/datasources/{:s}'.format(graf_host, lads_id)
        query = { 'name' : lads['name'], 'type' : lads['type'], 'access' : lads['access'], 'basicAuth': lads['basicAuth'] , 'basicAuthUser' : lads['user'] , 'basicAuthPassword' : lads['password'] }
        response = requests.put(url=url, json=query)
        print('  - Config access {:s} -> {:s}'.format(lads['name'], str(response.json())))

    print("\nGrafana : List of datasources")
    url = '{:s}/api/datasources'.format(graf_host)
    query = {  }
    response = requests.request("GET", url, params=query)
    for lads in response.json() :
        print('  -> {:s}'.format(str(lads)))

    # ---------- Grafana : Dashboards ----------
    print("\nGrafana : Creation of dashboards")
    i = 1
    while True :
        name_file = 'grafana-dashboard-{:d}.json'.format(i)
        fp = pathlib.Path(name_file)
        i += 1
        if not fp.exists() :
            break
        else :
            fps = fp.read_text()
            dashb = json.loads(fps)
            url = '{:s}/api/dashboards/db'.format(graf_host)
            query = { 'dashboard': dashb , 'folderId': 0 }
            query['dashboard']['id']  = None
            query['dashboard']['uid'] = None
            response = requests.post(url=url, json=query)
            print('  - {:s} -> {:s} '.format(name_file, str(response.json())))

    print("\nGrafana : Deleting default Admin")
    if admin_id is not None :
        admin_base_id = "1"
        url = '{:s}/api/admin/users/{:s}'.format(graf_host, admin_base_id)
        query = {  }
        response = requests.delete(url=url)
        print('  -> {:s}'.format(str(response.json())))

    exit()


