#!/usr/bin/env bash
# ----- INFLUXDB PORT -----
INFLUX_PORT=8086
# ----- SPECIFY ADMIN HERE -----
USER_ADMIN_NAME=idbroot
USER_ADMIN_PWD=toto
# ----- SPECIFY DATABASES HERE -----
DB_NAMES=( 'systemshealth' 'dataflow' )
DB_DURAT=( '30d'           '30d'      )
# ----- SPECIFY USERS HERE -----
USERS_READ_NAMES=( 'edgeviz'     )
USERS_READ_PWRDS=( 'toto2'       )
USERS_WRIT_NAMES=( 'idbwriter'   )
USERS_WRIT_PWRDS=( 'toto3'       )

# ----- Creation of the influxdb admin (should be done 1st) -----
echo ""
echo "--- Creation of Admin ${USER_ADMIN_NAME}"
curl -XPOST "http://influxdb:${INFLUX_PORT}/query" --data-urlencode "q=CREATE USER $USER_ADMIN_NAME WITH PASSWORD '$USER_ADMIN_PWD' WITH ALL PRIVILEGES"

# ----- Creation of the databases -----
echo ""
db_i=0
while [[ "${DB_NAMES[db_i]}" != '' ]]; do
    dbNam="${DB_NAMES[db_i]}"
    dbDur="${DB_DURAT[db_i]}"
    let db_i=db_i+1
    echo "--- Creation of Database ${dbNam} with a retention of ${dbDur}"
    curl -XPOST "http://influxdb:${INFLUX_PORT}/query?u=$USER_ADMIN_NAME&p=$USER_ADMIN_PWD" --data-urlencode "q=CREATE DATABASE ""${dbNam}"" WITH DURATION ${dbDur} NAME ""retention${dbDur}"" "
done

# ----- Creation of the READER users and grant rights on DBs -----
echo ""
usrr_i=0
while [[ "${USERS_READ_NAMES[usrr_i]}" != '' ]]; do
    usrNam="${USERS_READ_NAMES[usrr_i]}"
    usrPwd="${USERS_READ_PWRDS[usrr_i]}"
    let usrr_i=usrr_i+1
    # --- Creating the user ---
    echo "--- Creation of User ${usrNam}"
    curl -XPOST "http://influxdb:${INFLUX_PORT}/query?u=$USER_ADMIN_NAME&p=$USER_ADMIN_PWD" --data-urlencode "q=CREATE USER ${usrNam} WITH PASSWORD '${usrPwd}'"
    # --- Granting accesses on each DB ---
    db_i=0
    while [[ "${DB_NAMES[db_i]}" != '' ]]; do
        dbNam="${DB_NAMES[db_i]}"
        let db_i=db_i+1
        echo "--- Granting Read of ${usrNam} on ${dbNam}"
        curl -XPOST "http://influxdb:${INFLUX_PORT}/query?u=$USER_ADMIN_NAME&p=$USER_ADMIN_PWD" --data-urlencode "q=GRANT READ ON ""${dbNam}"" TO ""${usrNam}"" "
    done
done

# ----- Creation of the WRITER users and grant rights on DBs -----
echo ""
usrr_i=0
while [[ "${USERS_WRIT_NAMES[usrr_i]}" != '' ]]; do
    usrNam="${USERS_WRIT_NAMES[usrr_i]}"
    usrPwd="${USERS_WRIT_PWRDS[usrr_i]}"
    let usrr_i=usrr_i+1
    # --- Creating the user ---
    echo "--- Creation of User ${usrNam}"
    curl -XPOST "http://influxdb:${INFLUX_PORT}/query?u=$USER_ADMIN_NAME&p=$USER_ADMIN_PWD" --data-urlencode "q=CREATE USER ${usrNam} WITH PASSWORD '${usrPwd}'"
    # --- Granting accesses on each DB ---
    db_i=0
    while [[ "${DB_NAMES[db_i]}" != '' ]]; do
        dbNam="${DB_NAMES[db_i]}"
        let db_i=db_i+1
        echo "--- Granting Write of ${usrNam} on ${dbNam}"
        curl -XPOST "http://influxdb:${INFLUX_PORT}/query?u=$USER_ADMIN_NAME&p=$USER_ADMIN_PWD" --data-urlencode "q=GRANT WRITE ON ""${dbNam}"" TO ""${usrNam}"" "
    done
done

echo ""
echo "Finished"

# apt install -y influxdb-client
# influx -host 'influxdb' -port '8086' -username 'idbroot' -password 'toto'