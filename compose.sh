#!/bin/bash
action=$1
export COMPOSE_PROJECT_NAME=projectname
export COMPOSE_FILE=compose.yml

if [[ "$action" == "up" ]]; then
    sudo chmod -R a+w ./persistence/grafana
    # add -d to detach and get back the prompt --> use ./compose.sh down to stop
	docker-compose up --build --remove-orphans -d

elif [[ "$action" == "down" ]]; then
	docker-compose down --remove-orphans

elif [[ "$action" == "clean" ]]; then
    sudo rm -rf ./persistence/grafana/*
    sudo rm -rf ./persistence/influxdb/*
    sudo chmod -R a+w ./persistence/grafana

else
	echo "Possible parameters : clean | up | down"
	echo "clean will delete persistence files"
	exit 1

fi
